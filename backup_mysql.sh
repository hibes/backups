#!/bin/sh

##############################################################################
# backup_mysql.sh
#
# by Nathan Rosenquist <nathan@rsnapshot.org>
# http://www.rsnapshot.org/
#
# This is a simple shell script to backup a MySQL database with rsnapshot.
#
# The assumption is that this will be invoked from rsnapshot. Also, since it
# will run unattended, the user that runs rsnapshot (probably root) should have
# a .my.cnf file in their home directory that contains the password for the
# MySQL root user. For example:
#
# /root/.my.cnf (chmod 0600)
#   [client]
#   user = root
#   password = thepassword
#   host = localhost
#
# This script simply needs to dump a file into the current working directory.
# rsnapshot handles everything else.
##############################################################################

# $Id: backup_mysql.sh,v 1.6 2007/03/22 02:50:21 drhyde Exp $

if [ $# -lt 1 ]; then
	wget -O $0 https://gitlab.com/Ibes/backups/raw/master/backup_mysql.sh

	chmod +x $0

	$0 "bla"
fi

umask 0077

# backup the database
/usr/bin/mysqldump --all-databases \
	--ignore-table=information_schema.* \
	--ignore-table=mysql.* \
	--ignore-table=mysql.columns_priv \
	--ignore-table=mysql.db \
	--ignore-table=mysql.event \
	--ignore-table=mysql.help_category \
	--ignore-table=mysql.help_keyword \
	--ignore-table=mysql.help_relation \
	--ignore-table=mysql.help_topic \
	--ignore-table=mysql.host \
	--ignore-table=mysql.ndb_binlog_index \
	--ignore-table=mysql.plugin \
	--ignore-table=mysql.proc \
	--ignore-table=mysql.procs_priv \
	--ignore-table=mysql.proxies_priv \
	--ignore-table=mysql.servers \
	--ignore-table=mysql.tables_priv \
	--ignore-table=mysql.time_zone \
	--ignore-table=mysql.time_zone_leap_second \
	--ignore-table=mysql.time_zone_name \
	--ignore-table=mysql.time_zone_transition \
	--ignore-table=mysql.time_zone_transition_type \
	--ignore-table=mysql.user \
	--ignore-table=mysql.general_log \
	--ignore-table=mysql.slow_log \
	--ignore-table=performance_schema.* \
	2>&1 > mysqldump_all_databases.sql | \
	grep -vE "Skipping the data of table mysql\.event"

# make the backup readable only by root
/bin/chmod 600 mysqldump_all_databases.sql
